= Allow Duplicate =

The Allow Duplicate module allow user to decide to upload same file again or not.

The module provides a new setting on 'file' field setting page.
It verifies the file getting uploaded whether already exists or not. If exists it will return an error message.

== Requirements ==

  * File field

== Configuration ==

Go to 'manage fields' of content type.
Click on 'edit' of 'file' field.
Select 'Yes' or 'No' to allow user to upload duplicate file.